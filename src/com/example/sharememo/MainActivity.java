package com.example.sharememo;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity {
	// メンバ変数に保持することで複数のメソッドから利用できます
	private TextView mTextMemo;

	/**
	 * viewに関する処理が多いので、メソッドに分けた onCreateにすべて書いても問題はない。
	 * またこのクラス内から使うメソッドであるため、スコープはprivateである。
	 */
	private void initView() {
		// text_memoのViewを探してきて、メンバ変数に保持する
		mTextMemo = (TextView) findViewById(R.id.text_memo);

		// idがbutton_editのViewを探してくる
		Button buttonEdit = (Button) findViewById(R.id.button_edit);

		// idがbutton_shareのViewを探してくる
		Button buttonShare = (Button) findViewById(R.id.button_share);

		// ボタンにイベントを付ける
		// このボタンを押すことによってEditActivityを起動する。
		buttonEdit.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Toast.makeText(getApplicationContext(),
						"OnClickListener.onClick()", Toast.LENGTH_LONG).show();
				/* 　　この中の処理はボタンが押された時に実行される　　 */
				// インテントの作成
				Intent intent = new Intent(MainActivity.this,
						EditActivity.class);
				// インテントのExtraにedit_memo:mTextMemoの文字というセットを置く
				// これをすることで、EditActivityでintent.getStringExtra("edit_memo")と言った感じで取り出せる。
				intent.putExtra("edit_memo", mTextMemo.getText());

				// インテントからActivityをスタートさせる
				startActivityForResult(intent, 0);
				// startActivityForResultは結果が帰ってきた場合にonActivityResultを呼び出させる。
			}
		});

		// ボタンにイベントを付ける
		// このボタンを押すことによって暗黙のインテントでテキストとして共有する。
		buttonShare.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Toast.makeText(getApplicationContext(),
						"OnClickListener.onClick()", Toast.LENGTH_LONG).show();
				// 暗黙的インテントで、テキストを扱えるアプリを選択して起動させる
				Intent intent = new Intent();
				intent.setAction(Intent.ACTION_SEND);
				intent.setType("text/plain");
				intent.putExtra(Intent.EXTRA_TEXT, mTextMemo.getText()
						.toString());
				startActivity(intent);
			}
		});
	}

	/**
	 * 暗黙のインテントで受け取った時の処理 普通に起動した場合は関係なし、 共有からデータを受け取った場合にmTextMemoに文字をセットする
	 */
	private void receiveData() {
		// インテントから共有された値を受け取る
		Intent intent = getIntent();
		if (intent != null && intent.getAction() == Intent.ACTION_SEND
				&& intent.getType().equals("text/plain")
				&& intent.getStringExtra(Intent.EXTRA_TEXT) != null) {
			String intentText = intent.getStringExtra(Intent.EXTRA_TEXT);
			mTextMemo.setText(intentText);
		}
	}

	/**
	 * 一番最初に実行されるメソッド
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// トーストを出す
		Toast.makeText(getApplicationContext(), "MainActivity.onCreate()",
				Toast.LENGTH_LONG).show();
		// ActivityにLayoutを設定
		setContentView(R.layout.activity_main);
		// viewを初期化する・またイベントなどをつける
		initView();
		// intentからデータを取得・設定をする
		receiveData();
	}

	/**
	 * このメソッドはstartActivityForResult(intent)で開始したActivityが 終了して戻ってきた時に実行される。
	 * ちなみに終了して戻ってくるというのは Activityは開始するとスタックのようにつまれていき、
	 * finish()メソッドなどで終了させると前回開いていたActivityが表示されるため、
	 * 開始したActivity内でfinish()メソッドを呼ぶことで自動的に元のActivityに戻ってくる。
	 */
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		// トーストを出す
		Toast.makeText(getApplicationContext(),
				"MainActivity.onActivityResult()", Toast.LENGTH_LONG).show();
		// RequestコードとResultコードとintentが正しいか見る
		// 戻るキーで戻ってきた場合はintentがnullでくるので注意が必要
		if (requestCode == 0 && resultCode == RESULT_OK && data != null) {
			String memo = data.getStringExtra("memo");
			mTextMemo.setText(memo);
		}
	}

}
