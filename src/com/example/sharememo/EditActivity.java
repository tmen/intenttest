package com.example.sharememo;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class EditActivity extends Activity {
	private EditText mEditText;

	/**
	 * @see MainActivity#initView
	 */
	private void initView() {
		// idがedit_memoのViewをLayoutから探す
		mEditText = (EditText) findViewById(R.id.edit_memo);

		// idがbutton_endのViewをLayoutから探す
		Button buttonEnd = (Button) findViewById(R.id.button_end);
		// ボタンにクリックしたときのイベントをつける
		// クリックした時このActivityを終了し、MainActivityに戻る
		buttonEnd.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Toast.makeText(getApplicationContext(),
						"OnClickListener.onClick()", Toast.LENGTH_LONG).show();
				// MainActivity.onActivityResult()の引数で渡したいintentを設定する。
				Intent resultIntent = new Intent();
				// インテントにmemo:editTextの文字というセットを置く
				resultIntent.putExtra("memo", mEditText.getText().toString());
				// ResultコードにRESUKT_OKを指定する。
				// 任意の数字でOKだが、ここでは親クラスで定義されているRESULT_OKを利用する。
				setResult(RESULT_OK, resultIntent);
				// Activityを終了する
				finish();
			}
		});
	}

	/**
	 * @see MainActivity#receiveData
	 */
	private void receiveData() {
		// インテントの取得
		Intent intent = getIntent();
		// idがedit_memoをLayoutから探す
		mEditText = (EditText) findViewById(R.id.edit_memo);
		// intentから取り出す。
		mEditText.setText(intent.getStringExtra("edit_memo"));

	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Toast.makeText(getApplicationContext(), "EditActivity.onCreate()",
				Toast.LENGTH_LONG).show();
		setContentView(R.layout.activity_edit);
		// viewを初期化する・またイベントなどをつける
		initView();
		// intentからデータを取得・設定をする
		receiveData();
	}

}
